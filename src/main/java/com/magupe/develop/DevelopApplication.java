package com.magupe.develop;

import com.magupe.develop.rocketmq.Consumer;
import com.magupe.develop.rocketmq.Producer;
import org.apache.rocketmq.client.exception.MQClientException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author m1873
 */
@EnableScheduling
@SpringBootApplication
public class DevelopApplication {

    public static void main(String[] args) throws MQClientException {
        SpringApplication.run(DevelopApplication.class, args);

        Consumer.consumer();
    }

}
