package com.magupe.develop.jna;

import com.sun.jna.Library;
import com.sun.jna.Native;

/**
 * g++ -fpic -c library.cpp
 * g++ -shared -o libcodec.so library.o
 *
 * g++ -fpic -c library.cpp library.h
 * g++ -shared -o libcodec.so library.o
 */
public class CLibrary {

    public interface ICLibrary extends Library {
        ICLibrary INSTANCE = (ICLibrary) Native.loadLibrary("codec-process", ICLibrary.class);
        int sum(int a,int b);
    }

    public int sum(int a,int b){
        return ICLibrary.INSTANCE.sum(a,b);
    }

    public static void main(String[] args) {
        CLibrary cLibrary = new CLibrary();
        int c = cLibrary.sum(10, 14);
        System.out.println("10 + 14 = "+c);
    }
}
