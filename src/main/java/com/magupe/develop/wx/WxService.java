package com.magupe.develop.wx;

import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.WxMpTemplateMessage;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *
 * </p>
 *
 * @author magp
 * @date 2021/7/27
 */
@Service
public class WxService {

    @Resource
    private RedisTemplate redisTemplate;

    /**
     * 获取access_token
     *
     * @return
     */
    public String getToken(){
        String token = (String) redisTemplate.opsForValue().get("ums:wechat:token");
        if(StringUtils.isNotBlank(token)){
            return token;
        }
        WxMpInMemoryConfigStorage wxConfigProvider = new WxMpInMemoryConfigStorage();
        wxConfigProvider.setAppId("wxdc74264366806d10");
        wxConfigProvider.setSecret("5b7d08624834dfdfffb357d97694bb8e");
        WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(wxConfigProvider);
        try {
            token = wxMpService.getAccessToken();
            redisTemplate.opsForValue().set("ums:wechat:token", token, 2, TimeUnit.HOURS);
            return token;
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Test
    public void pushMsg(){
        WxMpInMemoryConfigStorage wxConfigProvider = new WxMpInMemoryConfigStorage();
        wxConfigProvider.setAppId("wxdc74264366806d10");
        wxConfigProvider.setSecret("5b7d08624834dfdfffb357d97694bb8e");
        WxMpService wxMpService = new WxMpServiceImpl();
        wxMpService.setWxMpConfigStorage(wxConfigProvider);

        WxMpTemplateMessage templateMessage = new WxMpTemplateMessage();
        templateMessage.setTemplateId("SODbWRciDEdr0prmjMWyijpn3BEBQgP60qfQmbmlHh8");
        templateMessage.setToUser("oYkUF6MzHOgXZOrd5b5X0KNxgyO8");
        templateMessage.setUrl("https://www.baidu.com");

        List<WxMpTemplateData> datas = templateMessage.getDatas();
        WxMpTemplateData dataName=new WxMpTemplateData("first","哈喽，又公布了一门成绩，快来看看吧！","#173177");
        WxMpTemplateData dataCourse=new WxMpTemplateData("keyword1","高等数学","#173177");
        WxMpTemplateData dataScore=new WxMpTemplateData("keyword2","99","#173177");
        WxMpTemplateData dataEnd=new WxMpTemplateData("remark","点击查看全部成绩","#173177");
        datas.add(dataCourse);
        datas.add(dataName);
        datas.add(dataScore);
        datas.add(dataEnd);

        try {
            wxMpService.templateSend(templateMessage);
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
    }
}
