package com.magupe.develop.sound;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.TargetDataLine;
import java.io.File;
import java.io.FileOutputStream;

public class PCMPlay {

    private static FileOutputStream os;
    //采样率
    private static float SAMPLE_RATE = 16000;
    //编码格式PCM
    private static AudioFormat.Encoding ENCODING = AudioFormat.Encoding.PCM_SIGNED;
    //帧大小 16
    private static int SAMPLE_SIZE = 16;
    //是否大端
    private static boolean BIG_ENDIAN = false;
    //通道数
    private static int CHANNELS = 1;

    public static void main(String[] args) throws Exception{
        if(args.length<1) {
            save("/data/home/magupe/Desktop/sound-16000.pcm");
        }else {
            save(args[0]);
        }
    }

    public static void save(String path) throws Exception {
        File file = new File(path);

        if(file.isDirectory()) {
            if(!file.exists()) {
                file.mkdirs();
            }
            file.createNewFile();
        }

        AudioFormat audioFormat = new AudioFormat(ENCODING, SAMPLE_RATE, SAMPLE_SIZE, CHANNELS, (SAMPLE_SIZE / 8) * CHANNELS, SAMPLE_RATE, BIG_ENDIAN);
        TargetDataLine targetDataLine = AudioSystem.getTargetDataLine(audioFormat);
        targetDataLine.open();
        targetDataLine.start();
        byte[] b = new byte[256];
        int flag = 0;
        os = new FileOutputStream(file);
        // 从声卡中采集数据
        while((flag = targetDataLine.read(b, 0, b.length))>0) {
            os.write(b);
        }
    }


}
