package com.magupe.develop.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.*;

public class HttpUtils {

    public static HttpResponse doGet(String url, Map<String, String> headers, Map<String, String> query) {
        try{
            HttpClient httpClient = wrapClient(url);
            HttpGet request = new HttpGet(buildUrl(url, query));
            for (Map.Entry<String, String> e : headers.entrySet()) {
                request.addHeader(e.getKey(), e.getValue());
            }
            return httpClient.execute(request);
        }catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    public static String doGet(String url){
        try {
            Map<String, String> headers = new HashMap<>();
            HttpResponse response = HttpUtils.doGet(url, headers, null);
            HttpEntity entity = response.getEntity();
            String resp = EntityUtils.toString(entity, "UTF-8");
            return resp;
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static String doGet(String url, Map<String, String> headers) {
        HttpResponse response = null;
        try{
            HttpClient httpClient = wrapClient(url);
            HttpGet request = new HttpGet(buildUrl(url, null));
            for (Map.Entry<String, String> e : headers.entrySet()) {
                request.addHeader(e.getKey(), e.getValue());
            }

            response = httpClient.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                return null;
            }

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream instream = entity.getContent();
                return IOUtils.toString(instream, "UTF-8");
            }
        } catch (IOException e){
            e.printStackTrace();
        } finally {
            if (response != null) {
                try {
                    EntityUtils.consume(response.getEntity());
                } catch (IOException e) {e.printStackTrace();}
            }
        }
        return null;
    }
    /**
     * form
     * @param url
     * @param headers
     * @param query
     * @param body
     * @return
     * @throws Exception
     */
    public static HttpResponse doPost(String url, Map<String, String> headers, Map<String, String> query, Map<String, String> body) throws Exception {
        HttpClient httpClient = wrapClient(url);
        HttpPost request = new HttpPost(buildUrl(url, query));
        for (Map.Entry<String, String> e : headers.entrySet()) {
            request.addHeader(e.getKey(), e.getValue());
        }
        if (body != null) {
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();

            for (String key : body.keySet()) {
                nameValuePairList.add(new BasicNameValuePair(key, body.get(key)));
            }
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(nameValuePairList, "utf-8");
            formEntity.setContentType("application/x-www-form-urlencoded; charset=UTF-8");
            request.setEntity(formEntity);
        }
        return httpClient.execute(request);
    }

    /**
     * string
     * @param url
     * @param headers
     * @param query
     * @param body
     * @return
     * @throws Exception
     */
    public static HttpResponse doPost(String url, Map<String, String> headers, Map<String, String> query, String body) {
        try{
            HttpClient httpClient = wrapClient(url);
            HttpPost request = new HttpPost(buildUrl(url, query));
            if(null != headers && !headers.isEmpty()){
                for (Map.Entry<String, String> e : headers.entrySet()) {
                    request.addHeader(e.getKey(), e.getValue());
                }
            }
            if (StringUtils.isNotBlank(body)) {
                request.setEntity(new StringEntity(body, "utf-8"));
            }
            return httpClient.execute(request);
        } catch (UnsupportedEncodingException e){
            e.printStackTrace();
        } catch (ClientProtocolException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * stream
     * @param url
     * @param headers
     * @param query
     * @param body
     * @return
     * @throws Exception
     */
    public static HttpResponse doPost(String url, Map<String, String> headers, Map<String, String> query, byte[] body) throws Exception {
        HttpClient httpClient = wrapClient(url);
        HttpPost request = new HttpPost(buildUrl(url, query));
        for (Map.Entry<String, String> e : headers.entrySet()) {
            request.addHeader(e.getKey(), e.getValue());
        }
        if (body != null) {
            request.setEntity(new ByteArrayEntity(body));
        }
        return httpClient.execute(request);
    }

    private static String buildUrl(String url, Map<String, String> queries) throws UnsupportedEncodingException {
        StringBuilder sbUrl = new StringBuilder();
        if (!StringUtils.isBlank(url)) {
            sbUrl.append(url);
        }
        if(!url.contains("?")){
            sbUrl.append("?");
        }
        if (null != queries) {
            StringBuilder builder = new StringBuilder();
            for (Map.Entry<String, String> query : queries.entrySet()) {
                builder.append(query.getKey());
                builder.append("=");
                builder.append(URLEncoder.encode(query.getValue(), "utf-8"));
                builder.append("&");
            }
            sbUrl.append("?").append(builder);
        }
        return sbUrl.toString();
    }

    public static HttpResponse doPut(String url, Map<String, String> headers, Map<String, String> query, String body) throws Exception {
        HttpClient httpClient = wrapClient(url);
        HttpPut request = new HttpPut(buildUrl(url, query));
        for (Map.Entry<String, String> e : headers.entrySet()) {
            request.addHeader(e.getKey(), e.getValue());
        }
        if (StringUtils.isNotBlank(body)) {
            request.setEntity(new StringEntity(body, "utf-8"));
        }
        return httpClient.execute(request);
    }

    public static HttpResponse doPut(String url, Map<String, String> headers, Map<String, String> query, byte[] body) throws Exception {
        HttpClient httpClient = wrapClient(url);
        HttpPut request = new HttpPut(buildUrl(url, query));
        for (Map.Entry<String, String> e : headers.entrySet()) {
            request.addHeader(e.getKey(), e.getValue());
        }
        if (body != null) {
            request.setEntity(new ByteArrayEntity(body));
        }
        return httpClient.execute(request);
    }

    public static HttpResponse doDelete(String url, Map<String, String> headers, Map<String, String> query)
            throws Exception {
        HttpClient httpClient = wrapClient(url);
        HttpDelete request = new HttpDelete(buildUrl(url, query));
        for (Map.Entry<String, String> e : headers.entrySet()) {
            request.addHeader(e.getKey(), e.getValue());
        }
        return httpClient.execute(request);
    }

    private static HttpClient wrapClient(String url) {
        HttpClient httpClient = HttpClientBuilder.create().build();
        if (url != null && url.startsWith("https://")) {
            return sslClient();
        }
        return httpClient;
    }

//    public static HttpClient sslClient(BasicCookieStore cookie) {
//        try {
//            // 在调用SSL之前需要重写验证方法，取消检测SSL
//            X509TrustManager trustManager = new X509TrustManager() {
//                @Override public X509Certificate[] getAcceptedIssuers() {
//                    return null;
//                }
//                @Override public void checkClientTrusted(X509Certificate[] xcs, String str) {}
//                @Override public void checkServerTrusted(X509Certificate[] xcs, String str) {}
//            };
//            SSLContext ctx = SSLContext.getInstance(SSLConnectionSocketFactory.TLS);
//            ctx.init(null, new TrustManager[] { trustManager }, null);
//            SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(ctx, NoopHostnameVerifier.INSTANCE);
//            // 创建Registry
//            RequestConfig requestConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD_STRICT)
//                    .setExpectContinueEnabled(Boolean.TRUE).setTargetPreferredAuthSchemes(Arrays.asList(AuthSchemes.NTLM,AuthSchemes.DIGEST))
//                    .setProxyPreferredAuthSchemes(Arrays.asList(AuthSchemes.BASIC)).build();
//            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
//                    .register("http", PlainConnectionSocketFactory.INSTANCE)
//                    .register("https",socketFactory).build();
//            // 创建ConnectionManager，添加Connection配置信息
//            PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
//            CloseableHttpClient closeableHttpClient = HttpClients.custom().setConnectionManager(connectionManager)
//                    .setDefaultRequestConfig(requestConfig).setDefaultCookieStore(cookie).build();
//            return closeableHttpClient;
//        } catch (KeyManagementException ex) {
//            throw new RuntimeException(ex);
//        } catch (NoSuchAlgorithmException ex) {
//            throw new RuntimeException(ex);
//        }
//    }

    public static HttpClient sslClient() {
        SSLContext ctx = getSSLContext();
        PoolingHttpClientConnectionManager connectionManager = getPoolingHttpClientConnectionManager(ctx);
        RequestConfig requestConfig = getRequestConfig();
        return sslClient(requestConfig, connectionManager);
    }

    public static HttpClient sslClient(BasicCookieStore cookie) {
        SSLContext ctx = getSSLContext();
        PoolingHttpClientConnectionManager connectionManager = getPoolingHttpClientConnectionManager(ctx);
        RequestConfig requestConfig = getRequestConfig();
        return sslClient(requestConfig, connectionManager, cookie);
    }

    public static SSLContext getSSLContext(){
        try {
            // 在调用SSL之前需要重写验证方法，取消检测SSL
            X509TrustManager trustManager = new X509TrustManager() {
                @Override public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                @Override public void checkClientTrusted(X509Certificate[] xcs, String str) {}
                @Override public void checkServerTrusted(X509Certificate[] xcs, String str) {}
            };
            SSLContext ctx = SSLContext.getInstance(SSLConnectionSocketFactory.TLS);
            ctx.init(null, new TrustManager[] { trustManager }, null);
            return ctx;
        } catch (KeyManagementException ex) {
            throw new RuntimeException(ex);
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static PoolingHttpClientConnectionManager getPoolingHttpClientConnectionManager(SSLContext ctx){
        SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(ctx, NoopHostnameVerifier.INSTANCE);
        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.INSTANCE)
                .register("https",socketFactory).build();
        // 创建ConnectionManager，添加Connection配置信息
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        return connectionManager;
    }

    public static RequestConfig getRequestConfig(){
        RequestConfig requestConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD_STRICT)
                .setExpectContinueEnabled(Boolean.TRUE).setTargetPreferredAuthSchemes(Arrays.asList(AuthSchemes.NTLM,AuthSchemes.DIGEST))
                .setProxyPreferredAuthSchemes(Arrays.asList(AuthSchemes.BASIC)).build();
        return requestConfig;
    }

    public static HttpClient sslClient(RequestConfig requestConfig, PoolingHttpClientConnectionManager connectionManager) {
        CloseableHttpClient closeableHttpClient = HttpClients.custom().setConnectionManager(connectionManager)
                .setDefaultRequestConfig(requestConfig).build();
        return closeableHttpClient;
    }

    public static HttpClient sslClient(RequestConfig requestConfig, PoolingHttpClientConnectionManager connectionManager, BasicCookieStore cookie) {
        CloseableHttpClient closeableHttpClient = HttpClients.custom().setConnectionManager(connectionManager)
                .setDefaultRequestConfig(requestConfig).setDefaultCookieStore(cookie).build();
        return closeableHttpClient;
    }

    public static JSONObject getJson(HttpResponse httpResponse) {
        if(null == httpResponse){
            return null;
        }
        try{
            HttpEntity entity = httpResponse.getEntity();
            String resp = EntityUtils.toString(entity, "UTF-8");
            if(resp != null && resp.length() == 0){
                return null;
            }
            EntityUtils.consume(entity);
            return JSON.parseObject(resp);
        } catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }
}
