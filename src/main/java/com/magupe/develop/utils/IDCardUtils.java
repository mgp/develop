package com.magupe.develop.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

public class IDCardUtils {

    /**
     * 省(直辖市)代码表
     */
    private static String PROVINCE_CODE[] = { "11", "12", "13", "14", "15", "21", "22",
            "23", "31", "32", "33", "34", "35", "36", "37", "41", "42", "43",
            "44", "45", "46", "50", "51", "52", "53", "54", "61", "62", "63",
            "64", "65", "71", "81", "82", "91" };

    public String validate(){
        return "";
    }

    /**
     * 验证身份证最后一位是否准确(只适合18位身份证)
     *
     * @param idNumber
     * @return
     */
    public static boolean validateLastNumber(String idNumber){
        if(idNumber.length() == 18){
            String lastNumber = idNumber.substring(idNumber.length() - 1, idNumber.length());
            idNumber = idNumber.substring(0, idNumber.length() -1);
            String lastNumber_ = getLastNumberBy18(idNumber);
            if(lastNumber.equals(lastNumber_)){
                return true;
            }
        }
        return false;
    }

    /**
     * 计算身份证最后一位数字(只适合18位身份证)
     *
     *  身份证的最后一位是根据前17位数字计算出来的检验码。计算方法是：将身份证号码前17位数分别乘以不同的系数。
     *  从第1位到第17位的系数分别为：7 9 10 5 8 4 2 1 6 3 7 9 10 5 8 4 2；将乘积之和除以11，余数可能为0 1 2 3 4 5 6 7 8 9 10。
     *  则根据余数，分别对应的最后一位身份证的号码为1 0 X 9 8 7 6 5 4 3 2。
     *  编写程序，输入身份证号码前17位，输出对应的检验码。
     * @param idNumber
     * @return
     */
    public static String getLastNumberBy18(String idNumber){
        int[] xi = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
        char[] contrast = {'1', '0', 'x', '9', '8', '7', '6', '5', '4', '3', '2'};
        int sum = 0;
        for (int i = 0; i < idNumber.length(); i++) {
            int per = idNumber.charAt(i) - 48;
            sum = sum + per * xi[i];
        }
        sum = sum % 11;
        return String.valueOf(contrast[sum]);
    }

    /**
     * 是否是身份证号
     *
     * @param cardid
     * @return
     */
    public static boolean isIdNumber(String cardid){
        return Pattern.matches("^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}([\\d|x|X]{1})$", cardid);
    }

    /**
     * 校验身份证日期
     *
     * @param inDate
     * @return
     */
    public boolean isValidDate(String inDate) {
        if (inDate == null){
            return false;
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        if (inDate.trim().length() != dateFormat.toPattern().length()){
            return false;
        }
        // 严格的日期匹配
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    /**
     * 检查身份证的省份信息是否正确（使用与18/15位身份证）
     * @param provinceid
     * @return
     */
    public boolean checkProvinceId(String provinceid){
        for (String id : PROVINCE_CODE) {
            if (id.equals(provinceid)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        boolean validate = validateLastNumber("410223199103116058");
        System.out.println(validate);
    }
}
