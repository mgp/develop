package com.magupe.develop.rocketmq;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;

/**
 *
 * @author
 */
public class Producer {

    public static void producer() throws MQClientException {
        DefaultMQProducer producer = new DefaultMQProducer("develop-group-test");
        // producer.setNamesrvAddr("192.168.2.163:10374");
        // producer.setNamesrvAddr("122.114.201.2:10374");
        producer.setNamesrvAddr("122.114.201.2:10374");



        producer.setInstanceName("rmq-instance");
        producer.start();


        try {
            for (int i = 1; i <= 100; i++){
                String msg = "消息" + i;
                System.out.println("生产者发送消息:"+ msg);
                producer.send(new Message("develop-test-topic", "develop-test-tag", msg.getBytes()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        producer.shutdown();
    }

    public static void main(String[] args) throws MQClientException {
        producer();
    }
}
