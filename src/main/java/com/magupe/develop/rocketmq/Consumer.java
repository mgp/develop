package com.magupe.develop.rocketmq;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

/**
 *
 * @author
 */
public class Consumer {

    public static void consumer() throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("develop-group-test");
        // consumer.setNamesrvAddr("192.168.217.129:10374");
        // consumer.setNamesrvAddr("192.168.2.163:10374");
        consumer.setNamesrvAddr("122.114.201.2:10374");
        consumer.setInstanceName("rmq-instance");
        consumer.subscribe("develop-test-topic", "develop-test-tag");
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt msg : msgs) {
                    System.out.println("消费者消费数据: " + new String(msg.getBody()));
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        consumer.start();
    }

    public static void main(String[] args) throws MQClientException {
        consumer();
    }
}
