package com.magupe.develop.config;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author m1873
 */
@Component
public class JedisClient {

	@Autowired
	private JedisPool jedisPool;

	public String set(String key, Object o) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.set(key, JSON.toJSONString(o));
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	public String set(String key, int seconds, String value) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.setex(key, seconds, value);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	// ------------------------ 1.对value操作的命令 ------------------------
	/**
	 * 确认一个key是否存在
	 * 
	 * @return
	 */
	public Boolean exists(String key) {
		Jedis jedis = jedisPool.getResource();
		Boolean str;
		try {
			str = jedis.exists(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;

	}

	/**
	 * 删除一个key
	 * 
	 * @return
	 */
	public Long del(String key) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.del(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * 返回值的类型
	 * 
	 * @return
	 */
	public String type(String key) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.type(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * 返回满足给定pattern的所有key
	 * 
	 * @return
	 */
	public Set<String> keys(String pattern) {
		Jedis jedis = jedisPool.getResource();
		Set<String> str;
		try {
			str = jedis.keys(pattern);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * 将key由oldname重命名为newname，若newname存在则删除newname表示的key
	 * 
	 * @return
	 */
	public String rename(String oldname, String newname) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.rename(oldname, newname);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * 返回当前数据库中key的数目
	 * 
	 * @return
	 */
	public Long dbsize() {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.dbSize();
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * expire：设定一个key的活动时间（s）
	 * 
	 * @param key
	 * @param seconds
	 * @return
	 */
	public Long expire(String key, int seconds) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.expire(key, seconds);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * 获得一个key的活动时间
	 * 
	 * @param key
	 * @return
	 */
	public Long ttl(String key) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.ttl(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * select(index)：按索引查询
	 * 
	 * @param index
	 * @return
	 */
	public String select(int index) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.select(index);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * move(key, dbindex)：将当前数据库中的key转移到有dbindex索引的数据库
	 * 
	 * @param key
	 * @param dbindex
	 * @return
	 */
	public Long move(String key, int dbindex) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.move(key, dbindex);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * flushdb：删除当前选择数据库中的所有key
	 * 
	 * @return
	 */
	public String flushdb() {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.flushDB();
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * flushall：删除所有数据库中的所有key
	 * 
	 * @return
	 */
	public String flushall() {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.flushAll();
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	// ------------------------ 2.对String操作的命令 ------------------------
	/**
	 * 给数据库中名称为key的string赋予值value
	 * 
	 * @return
	 */
	public String set(String key, String value) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.set(key, value);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * get(key)：返回数据库中名称为key的string的value
	 * 
	 * @param key
	 * @return
	 */
	public String get(String key) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.get(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * getset(key, value)：给名称为key的string赋予上一次的value
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public String getset(String key, String value) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.getSet(key, value);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * mget(key1, key2,…, key N)：返回库中多个string（它们的名称为key1，key2…）的value
	 * 
	 * @param key
	 * @return
	 */
	public List<String> mget(String... keys) {
		Jedis jedis = jedisPool.getResource();
		List<String> str;
		try {
			str = jedis.mget(keys);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * setnx(key, value)：如果不存在名称为key的string，则向库中添加string，名称为key，值为value
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public Long setnx(String key, String value) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.setnx(key, value);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * setex(key, time, value)：向库中添加string（名称为key，值为value）同时，设定过期时间time
	 * 
	 * @param key
	 * @param seconds
	 * @param value
	 * @return
	 */
	public String setex(String key, int seconds, String value) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.setex(key, seconds, value);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * mset(key1, value1, key2, value2,…key N, value N)：同时给多个string赋值，名称为key
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public String mset(String... keysvalues) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.mset(keysvalues);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * msetnx(key1, value1, key2, value2,…key N, value N)：如果所有名称为key  i的string都不存在，则向库中添加string，名称key i赋值为value i
	 * 
	 * @param keysvalues
	 * @return
	 */
	public Long msetnx(String... keysvalues) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.msetnx(keysvalues);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * incr(key)：名称为key的string增1操作
	 * 
	 * @return
	 */
	public Long incr(String key) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.incr(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * incrby(key, integer)：名称为key的string增加integer
	 * 
	 * @param key
	 * @param integer
	 * @return
	 */
	public Long incrby(String key, long integer) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.incrBy(key, integer);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * decr(key)：名称为key的string减1操作
	 * 
	 * @param key
	 * @return
	 */
	public Long decr(String key) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.decr(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * decrby(key, integer)：名称为key的string减少integer
	 * 
	 * @param key
	 * @param integer
	 * @return
	 */
	public Long decrby(String key, long integer) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.decrBy(key, integer);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * append(key, value)：名称为key的string的值附加value
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public Long append(String key, String value) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.append(key, value);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * substr(key, start, end)：返回名称为key的string的value的子串
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	public String substr(String key, int start, int end) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.substr(key, start, end);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	// ------------------------ 3.对List操作的命令 ------------------------
	/**
	 * rpush(key, value)：在名称为key的list尾添加一个值为value的元素
	 * 
	 * @param key
	 * @param strings
	 * @return
	 */
	public Long rpush(String key, String strings) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.rpush(key, strings);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * lpush(key, value)：在名称为key的list头添加一个值为value的 元素
	 * 
	 * @param key
	 * @param strings
	 * @return
	 */
	public Long lpush(String key, String strings) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.lpush(key, strings);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * llen(key)：返回名称为key的list的长度
	 * 
	 * @param key
	 * @return
	 */
	public Long llen(String key) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.llen(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * lrange(key, start, end)：返回名称为key的list中start至end之间的元素（下标从0开始，下同）
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	public List<String> lrange(String key, int start, int end) {
		Jedis jedis = jedisPool.getResource();
		List<String> str;
		try {
			str = jedis.lrange(key, start, end);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * ltrim(key, start, end)：截取名称为key的list，保留start至end之间的元素
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	public String ltrim(String key, int start, int end) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.ltrim(key, start, end);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * lindex(key, index)：返回名称为key的list中index位置的元素
	 * 
	 * @param key
	 * @param index
	 * @return
	 */
	public String lindex(String key, int index) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.lindex(key, index);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * lset(key, index, value)：给名称为key的list中index位置的元素赋值为value
	 * 
	 * @param key
	 * @param index
	 * @param value
	 * @return
	 */
	public String lset(String key, int index, String value) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.lset(key, index, value);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * lrem(key, count, value)：删除count个名称为key的list中值为value的元素。count为0，删除所有值为value的元素，count>0 从头至尾删除count个值为value的元素，count<0从尾到头删除|count|个值为value的元素。
	 * 
	 * @param key
	 * @param count
	 * @param value
	 * @return
	 */
	public Long lrem(String key, long count, String value) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.lrem(key, count, value);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * lpop(key)：返回并删除名称为key的list中的首元素
	 * 
	 * @param key
	 * @return
	 */
	public String lpop(String key) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.lpop(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * rpop(key)：返回并删除名称为key的list中的尾元素
	 * 
	 * @param key
	 * @return
	 */
	public String rpop(String key) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.rpop(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * blpop(key1, key2,… key N, timeout)：lpop 命令的block版本。即当timeout为0时，若遇到名称为key i的list不存在或该list为空，则命令结束。如果 timeout>0，则遇到上述情况时，等待timeout秒，如果问题没有解决，则对key i+1开始的list执行pop操作。
	 * 
	 * @param args
	 * @return
	 */
	public List<String> blpop(String... args) {
		Jedis jedis = jedisPool.getResource();
		List<String> str;
		try {
			str = jedis.blpop(args);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * brpop(key1, key2,… key N, timeout)：rpop的block版本。参考上一命令。
	 * 
	 * @param args
	 * @return
	 */
	public List<String> brpop(String... args) {
		Jedis jedis = jedisPool.getResource();
		List<String> str;
		try {
			str = jedis.brpop(args);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * rpoplpush(srckey,
	 * dstkey)：返回并删除名称为srckey的list的尾元素，并将该元素添加到名称为dstkey的list的头部
	 * 
	 * @param srckey
	 * @param dstkey
	 * @return
	 */
	public String rpoplpush(String srckey, String dstkey) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.rpoplpush(srckey, dstkey);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	// ------------------------ 4.对Set操作的命令 ------------------------
	/**
	 * sadd(key, member)：向名称为key的set中添加元素member
	 * 
	 * @param key
	 * @param members
	 * @return
	 */
	public Long sadd(String key, String... members) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.sadd(key, members);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * srem(key, member) ：删除名称为key的set中的元素member
	 * 
	 * @param key
	 * @param members
	 * @return
	 */
	public Long srem(String key, String... members) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.srem(key, members);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * spop(key) ：随机返回并删除名称为key的set中一个元素
	 * 
	 * @param key
	 * @return
	 */
	public String spop(String key) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.spop(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * smove(srckey, dstkey, member) ：将member元素从名称为srckey的集合移到名称为dstkey的集合
	 * 
	 * @param srckey
	 * @param dstkey
	 * @param member
	 * @return
	 */
	public Long smove(String srckey, String dstkey, String member) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.smove(srckey, dstkey, member);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * scard(key) ：返回名称为key的set的基数
	 * 
	 * @param key
	 * @return
	 */
	public Long scard(String key) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.scard(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * sismember(key, member) ：测试member是否是名称为key的set的元素
	 * 
	 * @param key
	 * @param member
	 * @return
	 */
	public Boolean sismember(String key, String member) {
		Jedis jedis = jedisPool.getResource();
		Boolean str;
		try {
			str = jedis.sismember(key, member);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * sinter(key1, key2,…key N) ：求交集
	 * 
	 * @param keys
	 * @return
	 */
	public Set<String> sinter(String... keys) {
		Jedis jedis = jedisPool.getResource();
		Set<String> str;
		try {
			str = jedis.sinter(keys);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * sinterstore(dstkey, key1, key2,…key N) ：求交集并将交集保存到dstkey的集合
	 * 
	 * @param dstkey
	 * @param keys
	 * @return
	 */
	public Long sinterstore(String dstkey, String... keys) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.sinterstore(dstkey, keys);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * sunion(key1, key2,…key N) ：求并集
	 * 
	 * @param keys
	 * @return
	 */
	public Set<String> sunion(String... keys) {
		Jedis jedis = jedisPool.getResource();
		Set<String> str;
		try {
			str = jedis.sunion(keys);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * sunionstore(dstkey, key1, key2,…key N) ：求并集并将并集保存到dstkey的集合
	 * 
	 * @param dstkey
	 * @param keys
	 * @return
	 */
	public Long sunionstore(String dstkey, String... keys) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.sunionstore(dstkey, keys);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * sdiff(key1, key2,…key N) ：求差集
	 * 
	 * @param keys
	 * @return
	 */
	public Set<String> sdiff(String... keys) {
		Jedis jedis = jedisPool.getResource();
		Set<String> str;
		try {
			str = jedis.sdiff(keys);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * sdiffstore(dstkey, key1, key2,…key N) ：求差集并将差集保存到dstkey的集合
	 * 
	 * @param dstkey
	 * @param keys
	 * @return
	 */
	public Long sdiffstore(String dstkey, String... keys) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.sdiffstore(dstkey, keys);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * smembers(key) ：返回名称为key的set的所有元素
	 * 
	 * @param key
	 * @return
	 */
	public Set<String> smembers(String key) {
		Jedis jedis = jedisPool.getResource();
		Set<String> str;
		try {
			str = jedis.smembers(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * srandmember(key) ：随机返回名称为key的set的一个元素
	 * 
	 * @param key
	 * @return
	 */
	public String srandmember(String key) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.srandmember(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	public List<String> srandmember(String key, int count) {
		Jedis jedis = jedisPool.getResource();
		List<String> str;
		try {
			str = jedis.srandmember(key, count);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	// ------------------------ 5.对zset（sorted set）操作的命令 ------------------------
	/**
	 * zadd(key, score, member)：向名称为key的zset中添加元素member，score用于排序。如果该元素已经存在，则根据score更新该元素的顺序。
	 * 
	 * @return
	 */
	public Long zadd(String key, Double score, String member) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.zadd(key, score, member);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * zrem(key, member) ：删除名称为key的zset中的元素member
	 * 
	 * @param key
	 * @param members
	 * @return
	 */
	public Long zrem(String key, String... members) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.zrem(key, members);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * zincrby(key, increment, member)：如果在名称为key的zset中已经存在元素member，则该元素的score增加increment；否则向集合中添加该元素， 其score的值为increment
	 * 
	 * @param key
	 * @param score
	 * @param member
	 * @return
	 */
	public Double zincrby(String key, Double score, String member) {
		Jedis jedis = jedisPool.getResource();
		Double str;
		try {
			str = jedis.zincrby(key, score, member);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * zrank(key, member)：返回名称为key的zset（元素已按score从小到大排序）中member元素的rank（即index，从0开始），若没有member元素，返回“nil”
	 * @param key
	 * @param member
	 * @return
	 */
	public Long zrank(String key, String member) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.zrank(key, member);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * zrevrank(key, member)：返回名称为key的zset（元素已按score从大到小排序）中member元素的rank（即index，从0开始）， 若没有member元素，返回“nil”
	 * 
	 * @param key
	 * @param member
	 * @return
	 */
	public Long zrevrank(String key, String member) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.zrevrank(key, member);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * zrange(key, start, end)：返回名称为key的zset（元素已按score从小到大排序）中的index从start到end的所有元素
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	public Set<String> zrange(String key, Long start, Long end) {
		Jedis jedis = jedisPool.getResource();
		Set<String> str;
		try {
			str = jedis.zrange(key, start, end);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * zrevrange(key, start, end)：返回名称为key的zset（元素已按score从大到小排序）中的index从start到end的所有元素
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	public Set<String> zrevrange(String key, long start, long end) {
		Jedis jedis = jedisPool.getResource();
		Set<String> str;
		try {
			str = jedis.zrevrange(key, start, end);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * zrangebyscore(key, min, max)：返回名称为key的zset中score >= min且score <= max的所有元素
	 * 
	 * @param key
	 * @param min
	 * @param max
	 * @return
	 */
	public Set<String> zrangebyscore(String key, double min, double max) {
		Jedis jedis = jedisPool.getResource();
		Set<String> str;
		try {
			str = jedis.zrangeByScore(key, min, max);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * zcard(key)：返回名称为key的zset的基数
	 * 
	 * @param key
	 * @return
	 */
	public Long zcard(String key) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.zcard(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * zscore(key, element)：返回名称为key的zset中元素element的score
	 * 
	 * @param key
	 * @param member
	 * @return
	 */
	public Double zscore(String key, String member) {
		Jedis jedis = jedisPool.getResource();
		Double str;
		try {
			str = jedis.zscore(key, member);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * zremrangebyrank(key, min, max)：删除名称为key的zset中rank >= min且rank <= max的所有元素
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	public Long zremrangebyrank(String key, long start, long end) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.zremrangeByRank(key, start, end);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * zremrangebyscore(key, min, max) ：删除名称为key的zset中score >= min且score <=
	 * max的所有元素
	 * 
	 * @param key
	 * @param start
	 * @param end
	 * @return
	 */
	public Long zremrangebyscore(String key, double start, double end) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.zremrangeByScore(key, start, end);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * zunionstore / zinterstore(dstkeyN, key1,…,keyN, WEIGHTS w1,…wN, AGGREGATE SUM|MIN|MAX)：对N个zset求并集和交集，并将最后的集合保存在dstkeyN中。对于集合中每一个元素的score，
	 * 在进行AGGREGATE运算前，都要乘以对于的WEIGHT参数。如果没有提供WEIGHT，默认为1。默认的AGGREGATE是SUM， 即结果集合中元素的score是所有集合对应元素进行,
	 * SUM运算的值，而MIN和MAX是指，结果集合中元素的score是所有集合对应元素中最小值和最大值。
	 * 
	 * @param dstkey
	 * @param sets
	 * @return
	 */
	public Long zunionstore(String dstkey, String... sets) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.zunionstore(dstkey, sets);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	// ------------------------ 6.对Hash操作的命令 ------------------------
	/**
	 * hset(key, field, value)：向名称为key的hash中添加元素field<—>value
	 * 
	 * @param key
	 * @param field
	 * @param value
	 * @return
	 */
	public Long hset(String key, String field, String value) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.hset(key, field, value);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * hget(key, field)：返回名称为key的hash中field对应的value
	 * @param key
	 * @param field
	 * @return
	 */
	public String hget(String key, String field) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.hget(key, field);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * hmget(key, field1, …,field N)：返回名称为key的hash中field i对应的value
	 * 
	 * @param key
	 * @param fields
	 * @return
	 */
	public List<String> hmget(String key, String... fields) {
		Jedis jedis = jedisPool.getResource();
		List<String> str;
		try {
			str = jedis.hmget(key, fields);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * hmset(key, field1, value1,…,field N, value N)：向名称为key的hash中添加元素field i
	 * <—>value i
	 * 
	 * @param key
	 * @param hash
	 * @return
	 */
	public String hmset(String key, Map<String, String> hash) {
		Jedis jedis = jedisPool.getResource();
		String str;
		try {
			str = jedis.hmset(key, hash);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * hincrby(key, field, integer)：将名称为key的hash中field的value增加integer
	 * 
	 * @param key
	 * @param field
	 * @param value
	 * @return
	 */
	public Long hincrby(String key, String field, long value) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.hincrBy(key, field, value);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * hexists(key, field)：名称为key的hash中是否存在键为field的域
	 * 
	 * @param key
	 * @param field
	 * @return
	 */
	public Boolean hexists(String key, String field) {
		Jedis jedis = jedisPool.getResource();
		Boolean str;
		try {
			str = jedis.hexists(key, field);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * hdel(key, field)：删除名称为key的hash中键为field的域
	 * 
	 * @param key
	 * @param fields
	 * @return
	 */
	public Long hdel(String key, String... fields) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.hdel(key, fields);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * hlen(key)：返回名称为key的hash中元素个数
	 * 
	 * @param key
	 * @return
	 */
	public Long hlen(String key) {
		Jedis jedis = jedisPool.getResource();
		Long str;
		try {
			str = jedis.hlen(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * hkeys(key)：返回名称为key的hash中所有键
	 * 
	 * @param key
	 * @return
	 */
	public Set<String> hkeys(String key) {
		Jedis jedis = jedisPool.getResource();
		Set<String> str;
		try {
			str = jedis.hkeys(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * hvals(key)：返回名称为key的hash中所有键对应的value
	 * 
	 * @param key
	 * @return
	 */
	public List<String> hvals(String key) {
		Jedis jedis = jedisPool.getResource();
		List<String> str;
		try {
			str = jedis.hvals(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	/**
	 * hgetall(key)：返回名称为key的hash中所有的键（field）及其对应的value
	 * 
	 * @param key
	 * @return
	 */
	public Map<String, String> hgetall(String key) {
		Jedis jedis = jedisPool.getResource();
		Map<String, String> str;
		try {
			str = jedis.hgetAll(key);
		} finally {
			try {
				jedis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return str;
	}

}
