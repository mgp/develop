package com.magupe.develop.data.linked;

public class Node2 {

    public int value;    // 数据
    public Node2 next;   // 下一节点

    public Node2(){}

    public Node2(int value){
        this.value = value;
    }

    public Node2(int value, Node2 next){
        this.value =value;
        this.next = next;
    }

    public int getValue(){
        return value;
    }

    public Node2 setValue(int value){
        this.value = value;
        return this;
    }

    public Node2 getNext(){
        return this.next;
    }

    public Node2 setNext(Node2 next){
        this.next = next;
        return this;
    }

    public static void main(String[] args) {
        // 第一个节点
        Node2 first = new Node2(1);

        // 第二个节点
        Node2 second = new Node2(2);

        // 第三个节点
        Node2 third = new Node2(3);

        // 第四个节点
        Node2 fourth = new Node2(4);

        // 链接
        first.setNext(second);
        second.setNext(third);
        third.setNext(fourth);

        System.out.println(first.getNext().getNext().getNext().getValue());
    }
}
