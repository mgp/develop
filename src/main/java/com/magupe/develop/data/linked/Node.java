package com.magupe.develop.data.linked;

public class Node <T> {

    public T value;         // 数据
    public Node next;       // 下一节点

    public Node(){}

    public Node(T value){
        this.value = value;
    }

    public boolean hasNext(){
        return this.next != null ? true : false;
    }

    // 尾部插入
    public void addToTail(Node node){
        Node temp = this;
        while(temp.hasNext()){
            temp = temp.next;
        }
        temp.next = node;
    }

    // 头部插入
    public void addToHead(Node head, Node node){
        node.next = head.next;
        head.next = node;
    }

    // 从某个节点插入
    public Node addToNext(Node node){
        node.next = this.next;
        this.next = node;
        return node;
    }

    // 链表逆转
    public static Node reverse(Node first){
        Node cur = first;      // 前驱节点
        Node pre = null;       // 当前节点
        while(cur.hasNext()){
            Node after = cur.next;          // 标记出来下一个操作的节点
            cur.next = pre;                 // 当前节点指向前驱节点
            pre = cur;                      // 现有当前节点改为前驱节点
            cur = after;                    // 现有下一个节点 改为当前需要操作的节点
        }
        cur.next = pre;                     // 到最末尾的时候直接
        return cur;
    }

    @Override
    public String toString() {
        String val = this.value.toString();
        if(this.next != null){
            val = val.concat(" hasNext ");
            val = val.concat(this.next.toString());
        }
        return val;
    }

    public static void main(String[] args) {
        // 第一个节点
        Node first = new Node(1);

        // 第二个节点
        Node second = new Node(2);

        // 第三个节点
        Node third = new Node(3);

        // 第四个节点
        Node fourth = new Node(4);

        // 链接
        first.addToNext(second).addToNext(third).addToNext(fourth);

        System.out.println(first);
        System.out.println(reverse(first));
    }
}
