package com.magupe.develop.data.linked;

public class Node1 {

    public int value;    // 数据
    public Node1 next;   // 下一节点

    public int getValue(){
        return value;
    }

    public Node1 setValue(int value){
        this.value = value;
        return this;
    }

    public Node1 getNext(){
        return this.next;
    }

    public Node1 setNext(Node1 next){
        this.next = next;
        return this;
    }

    public static void main(String[] args) {
        // 第一个节点
        Node1 first = new Node1();
        first.setValue(1);
        first.setNext(null);

        // 第二个节点
        Node1 second = new Node1();
        second.setValue(2);
        second.setNext(null);

        // 第三个节点
        Node1 third = new Node1();
        third.setValue(3);
        third.setNext(null);

        // 第四个节点
        Node1 fourth = new Node1();
        fourth.setValue(4);
        fourth.setNext(null);

        // 链接
        first.setNext(second);
        second.setNext(third);
        third.setNext(fourth);

        System.out.println(first.getNext().getNext().getNext().getValue());
    }
}
