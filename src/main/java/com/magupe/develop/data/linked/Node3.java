package com.magupe.develop.data.linked;

public class Node3 {

    public int value;    // 数据
    public Node3 next;   // 下一节点

    public Node3(){}

    public Node3(int value){
        this.value = value;
    }

    public int getValue(){
        return value;
    }

    public Node3 getNext(){
        return this.next;
    }

    public Node3 setNext(Node3 node){
        this.next = node;
        return this;
    }

    public Node3 add(Node3 node){
        this.next = node;
        return node;
    }

    public boolean hasNext(){
        return this.next != null ? true : false;
    }

    @Override
    public String toString() {
        String val = Integer.toString(this.value);
        if(this.next != null){
            val = val.concat(" hasNext ");
            val = val.concat(this.next.toString());
        }
        return val;
    }

    public static void main(String[] args) {
        // 第一个节点
        Node3 first = new Node3(1);

        // 第二个节点
        Node3 second = new Node3(2);

        // 第三个节点
        Node3 third = new Node3(3);

        // 第四个节点
        Node3 fourth = new Node3(4);

        // 链接
        first.setNext(second);
        second.setNext(third);
        third.setNext(fourth);

        //System.out.println(first.getNext().getNext().getNext().getValue());
        System.out.println(first);
    }
}
