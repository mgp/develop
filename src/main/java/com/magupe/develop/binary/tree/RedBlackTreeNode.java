package com.magupe.develop.binary.tree;

/**
 * @author
 */
public class RedBlackTreeNode <T> {

    T data;
    RedBlackTreeNode<T> left;
    RedBlackTreeNode<T> right;
    RedBlackTreeNode<T> parent;
    boolean color;

    public RedBlackTreeNode(T data, boolean color, RedBlackTreeNode<T> parent, RedBlackTreeNode<T> left, RedBlackTreeNode<T> right) {
        this.data = data;
        this.color = color;
        this.parent = parent;
        this.left = left;
        this.right = right;
    }
}
