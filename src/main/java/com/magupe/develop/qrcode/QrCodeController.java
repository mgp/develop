package com.magupe.develop.qrcode;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

/**
 *
 * @author
 */
@Controller
public class QrCodeController {

    @RequestMapping(value = "/createCommonQRCode")
    public void createCommonQRCode(HttpServletResponse response, String text) throws Exception {
        ServletOutputStream stream = null;
        try {
            stream = response.getOutputStream();
            QrCodeGenerator.encode(text, stream);
        } catch (Exception e) {
            e.getStackTrace();
        } finally {
            if (stream != null) {
                stream.flush();
                stream.close();
            }
        }
    }

    @RequestMapping(value = "/createLogoQRCode")
    public void createLogoQRCode(HttpServletResponse response, String text) throws Exception {
        ServletOutputStream stream = null;
        try {
            stream = response.getOutputStream();
            String logo = Thread.currentThread().getContextClassLoader().getResource("").getPath() + "templates" + File.separator + "logo.png";
            QrCodeGenerator.encode(text, logo, stream, true);
        } catch (Exception e) {
            e.getStackTrace();
        } finally {
            if (stream != null) {
                stream.flush();
                stream.close();
            }
        }
    }
}
