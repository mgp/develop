package com.magupe.develop.qrcode;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.UUID;

/**
 *
 * @author
 */
public class QrCodeUtil {
    /**
     * 二维码尺寸
     */
    private static final int QR_CODE_SIZE = 300;

    /**
     * 生成二维码
     *
     * @param content 二维码的内容
     * @param path    输出路径
     */
    public static void createQrCode(String content, String path) {
        // 1、设置二维码的一些参数
        HashMap hints = new HashMap();
        // 1.1设置字符集
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        // 1.2设置容错等级；因为有了容错，在一定范围内可以把二维码p成你喜欢的样式
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        // 1.3设置外边距;(即白色区域)
        hints.put(EncodeHintType.MARGIN, 1);
        // 2、生成二维码
        try {
            // 2.1定义BitMatrix对象
            BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, QR_CODE_SIZE, QR_CODE_SIZE, hints);
            // 2.2、设置二维码存放路径,以及二维码的名字
            Path codePath = new File(path + UUID.randomUUID() + ".png").toPath();
            // 2.3、执行生成二维码
            MatrixToImageWriter.writeToPath(bitMatrix, "png", codePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 生成中间有logo的二维码
     *
     * @param content 二维码的内容
     * @param path    输出路径
     * @param imgPath logo路径
     */
    public static void createLogoQrCode(String content, String path, String imgPath) {
        // 1、设置二维码的一些参数
        HashMap hints = new HashMap();
        // 1.1设置字符集
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        // 1.2设置容错等级；因为有了容错，在一定范围内可以把二维码p成你喜欢的样式
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        // 1.3设置外边距;(即白色区域)
        hints.put(EncodeHintType.MARGIN, 1);
        // 2、生成二维码
        try {
            // 2.1定义BitMatrix对象
            BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, QR_CODE_SIZE, QR_CODE_SIZE, hints);
            // 2.2、设置二维码存放路径,以及二维码的名字
            File qrFile = new File(path, UUID.randomUUID() + ".jpg");
            // 2.3、执行生成二维码
            MatrixToImageWriter.writeToPath(bitMatrix, "jpg", qrFile.toPath());
            // 2.4在二维码中添加logo
            addLogo(qrFile, new File(imgPath), new LogoConfig());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param qrPic 二维码文件路径
     * @param logoPic logo文件路径
     * @param logoConfig 配置类
     */
    private static void addLogo(File qrPic, File logoPic, LogoConfig logoConfig) {
        if (!qrPic.isFile() || !logoPic.isFile()) {
            System.exit(0);
        }
        try {
            // 1、读取二维码图片，并构建绘图对象
            BufferedImage image = ImageIO.read(qrPic);
            Graphics2D graph = image.createGraphics();
            // 2、读取logo图片
            BufferedImage logo = ImageIO.read(logoPic);
            int widthLogo = image.getWidth() / logoConfig.getLogoPart();
            int heightLogo = image.getHeight() / logoConfig.getLogoPart();
            // 3、计算图片放置的位置
            int x = (image.getWidth() - widthLogo) / 2;
            int y = (image.getHeight() - heightLogo) / 2;
            // 4、绘制图片
            graph.drawImage(logo, x, y, widthLogo, heightLogo, null);
            graph.drawRoundRect(x, y, widthLogo, heightLogo, 10, 10);
            graph.setStroke(new BasicStroke(logoConfig.getBorder()));
            graph.setColor(logoConfig.getBorderColor());
            graph.drawRect(x, y, widthLogo, heightLogo);
            graph.dispose();
            ImageIO.write(image, "png", qrPic);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        // QrCodeUtil.createQrCode("1-1-0", "C:/Users/m1873/Desktop/qrCode/");
        QrCodeUtil.createLogoQrCode("1-1-0", "C:/Users/m1873/Desktop/qrCode/", "C:/Users/m1873/Desktop/qrCode/logo.png");
    }
}

class LogoConfig {
    /**
     * logo默认边框颜色
     */
    public static final Color DEFAULT_BORDER_COLOR = Color.WHITE;
    /**
     * logo默认边框宽度
     */
    public static final int DEFAULT_BORDER = 2;
    /**
     * logo大小默认为照片的1/6
     */
    public static final int DEFAULT_LOGO_PART = 6;

    private final int border = DEFAULT_BORDER;
    private final Color borderColor;
    private final int logoPart;

    public LogoConfig() {
        this(DEFAULT_BORDER_COLOR, DEFAULT_LOGO_PART);
    }

    public LogoConfig(Color borderColor, int logoPart) {
        this.borderColor = borderColor;
        this.logoPart = logoPart;
    }

    public Color getBorderColor() {
        return borderColor;
    }

    public int getBorder() {
        return border;
    }

    public int getLogoPart() {
        return logoPart;
    }
}
