package com.magupe.develop.udp.chat.server;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import io.netty.util.CharsetUtil;

public class UdpChatServerHandler extends SimpleChannelInboundHandler<DatagramPacket> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DatagramPacket packet) throws Exception {
        System.out.println(packet.sender());
        System.out.println(packet.sender().getAddress());
        System.out.println(packet.sender().getHostName());
        System.out.println(packet.sender().getHostString());
        System.out.println(packet.sender().getPort());
        System.out.println(packet.content().toString(CharsetUtil.UTF_8));

        //消息发送
        DatagramPacket dp = new DatagramPacket(Unpooled.copiedBuffer("123456".getBytes()), packet.sender());
        UdpChatServer.channel.writeAndFlush(dp);
    }
}
